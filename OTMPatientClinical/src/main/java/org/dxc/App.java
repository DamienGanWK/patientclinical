package org.dxc;


import java.util.ArrayList;
import java.util.Date;

import org.dxc.entity.Clinical;
import org.dxc.entity.Patient;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Hello world!
 *
 */
public class App {
	private static SessionFactory factory;

	public static void main(String[] args) {
		try {
			// factory obj created
			factory = new Configuration().configure("resource/hibernate.cfg.xml").buildSessionFactory();
		} catch (Throwable e) {
			System.err.println("Failed to create Session");
			throw new ExceptionInInitializerError(e);
		}
		Session session = factory.openSession();
		org.hibernate.Transaction tx = session.beginTransaction();
		
		Clinical c1 = new Clinical();
		c1.setComponentname("Height");
		c1.setComponentValue(175.0);
		c1.setMeasuredDateTime(new Date());
		
		Clinical c2 = new Clinical();
		c2.setComponentname("Weight");
		c2.setComponentValue(70.0);
		c2.setMeasuredDateTime(new Date());
		
		Clinical c3 = new Clinical();
		c3.setComponentname("Height");
		c3.setComponentValue(165.0);
		c3.setMeasuredDateTime(new Date());
		
		Clinical c4 = new Clinical();
		c4.setComponentname("Weight");
		c4.setComponentValue(70.0);
		c4.setMeasuredDateTime(new Date());
		
		
		ArrayList<Clinical> list1 = new ArrayList<Clinical>();
		list1.add(c1);list1.add(c2);
		
		ArrayList<Clinical> list2 = new ArrayList<Clinical>();
		list2.add(c3);list2.add(c4);
		
		Patient patient1 = new Patient();
		patient1.setFname("Damien");
		patient1.setLname("Gan");
		patient1.setAge(28);
		patient1.setAnswers(list1);
		
		Patient patient2 = new Patient();
		patient2.setFname("Ken");
		patient2.setLname("Lim");
		patient2.setAge(28);
		patient2.setAnswers(list2);
		
		session.persist(patient1);
		session.persist(patient2);
		
		tx.commit();
		session.close();
		System.out.println("Success");
		
		
		
		
		
	}
}
