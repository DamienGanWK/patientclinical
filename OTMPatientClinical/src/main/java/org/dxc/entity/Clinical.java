package org.dxc.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
//@Table(name="answer")
public class Clinical {
	
	@Id
	@Column(unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String componentName;
	private Double componentValue;
	private Date measuredDateTime;

	
	public Double getComponentValue() {
		return componentValue;
	}
	public void setComponentValue(Double componentValue) {
		this.componentValue = componentValue;
	}
	public Clinical() {
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getComponentname() {
		return componentName;
	}
	public void setComponentname(String componentName) {
		this.componentName = componentName;
	}
	
	public Date getMeasuredDateTime() {
		return measuredDateTime;
	}
	
	public void setMeasuredDateTime(Date measuredDateTime) {
		this.measuredDateTime = measuredDateTime;
	}

}
