package org.dxc.entity;

import java.util.List;

import javax.persistence.*;

@Entity
//@Table(name = "question")

public class Patient {
	@Id
	@Column(unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String fname, lname;
	private int age;

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "patientid")
	@OrderColumn(name = "type")
	private List<Clinical> answers;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public List<Clinical> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Clinical> answers) {
		this.answers = answers;
	}

//	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
//	private int id;
//	private String fname;
//	private String lname;
//	private int age;
//	
//	@OneToMany(cascade=CascadeType.ALL)
//	@JoinColumn(name="id")
//	@OrderColumn(name="type")
//	private List<Answer> answers;



}
