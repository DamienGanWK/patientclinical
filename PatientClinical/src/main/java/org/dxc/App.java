package org.dxc;

import java.util.Date;

import org.dxc.entity.Clinical;
import org.dxc.entity.Patient;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * Hello world!
 *
 */
public class App {
	private static SessionFactory factory;

	public static void main(String[] args) {

		try {
			// factory obj created
			factory = new Configuration().configure("resource/hibernate.cfg.xml").buildSessionFactory();
		} catch (Throwable e) {
			System.err.println("Failed to create Session");
			throw new ExceptionInInitializerError(e);
		}
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();

		Patient p1 = new Patient();
		p1.setFirstname("Damien");
		p1.setLastname("Gan");
		p1.setAge(28);

		Patient p2 = new Patient();
		p2.setFirstname("Ken");
		p2.setLastname("Lim");
		p2.setAge(28);

		Clinical clinical1 = new Clinical();
		clinical1.setComponentName("Height");
		clinical1.setComponentValue(175);
		clinical1.setMeasuredDateTime(java.sql.Date.valueOf(java.time.LocalDate.now()));

		Clinical clinical2 = new Clinical();
		clinical2.setComponentName("Weight");
		clinical2.setComponentValue(60);
		clinical2.setMeasuredDateTime(java.sql.Date.valueOf(java.time.LocalDate.now()));

		p1.setClinical(clinical1);
		clinical1.setPatient(p1);

		p2.setClinical(clinical2);
		clinical2.setPatient(p2);

		session.persist(p1);
		session.persist(p2);

		tx.commit();

		session.close();
		System.out.println("success");
	}
}
