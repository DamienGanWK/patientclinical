package org.dxc.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Clinical {
	@Id
	@Column(unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int Id;
	private String componentName;
	private int componentValue;
	private Date measuredDateTime;

	@OneToOne(targetEntity = Patient.class)
	private Patient patient;

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}


	public Date getMeasuredDateTime() {
		return measuredDateTime;
	}

	public void setMeasuredDateTime(Date measuredDateTime) {
		this.measuredDateTime = measuredDateTime;
	}

	public int getComponentValue() {
		return componentValue;
	}

	public void setComponentValue(int componentValue) {
		this.componentValue = componentValue;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

}
